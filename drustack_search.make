api = 2
core = 7.x

; Modules
projects[apachesolr][download][tag] = 7.x-1.7
projects[apachesolr][download][type] = git
projects[apachesolr][subdir] = contrib
projects[facetapi][download][tag] = 7.x-1.5
projects[facetapi][download][type] = git
projects[facetapi][patch][1616518] = https://drupal.org/files/1616518-7-show-active-term.patch
projects[facetapi][subdir] = contrib
projects[search_api][download][tag] = 7.x-1.14
projects[search_api][download][type] = git
projects[search_api][subdir] = contrib
projects[search_api_db][download][tag] = 7.x-1.4
projects[search_api_db][download][type] = git
projects[search_api_db][subdir] = contrib
projects[search_api_ranges][download][tag] = 7.x-1.5
projects[search_api_ranges][download][type] = git
projects[search_api_ranges][subdir] = contrib
projects[search_api_sorts][download][tag] = 7.x-1.5
projects[search_api_sorts][download][type] = git
projects[search_api_sorts][subdir] = contrib
